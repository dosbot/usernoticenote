package com.itwookie.plugin_dosbot.usernote;

import com.itwookie.dosbot.eventBus.Subscribe;
import com.itwookie.dosbot.events.chat.UserNoticeEvent;
import com.itwookie.pluginloader.PluginContainer;
import com.itwookie.pluginloader.WookiePluginAnnotation;
import com.itwookie.pluginloader.WookiePluginFunction;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

import static com.itwookie.pluginloader.WookiePluginFunction.Type.onLoaded;
import static com.itwookie.pluginloader.WookiePluginFunction.Type.onUnload;

@WookiePluginAnnotation(
        Name = "UserNoticeNote",
        Description = "Show user notices like subscriptions in a system notification",
        Author = "DosMike",
        Version = "1.0"
)
public class Plugin {
    TrayIcon tray = null;

    @WookiePluginFunction(onLoaded)
    public void onLoaded(PluginContainer me) {
        SystemTray systray = SystemTray.getSystemTray();
        Image img = null;
        try {
            img = ImageIO.read(me.getResource("assets/UserNoticeNote/tray.png"));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
//        Image img = Toolkit.getDefaultToolkit().createImage("assets/UserNoticeNote/tray.png");
        TrayIcon trayIcon = new TrayIcon(img, "DosBot - Twitch");
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip("Tray Icon for DosBot for Twitch");
        try {
            systray.add(trayIcon);
            tray = trayIcon;
        } catch (AWTException e) {
            System.err.println("Tray Icon did not work");
            e.printStackTrace();
        }
    }

    @WookiePluginFunction(onUnload)
    public void onUnload() {
        if (tray != null)
            SystemTray.getSystemTray().remove(tray);
    }

    @Subscribe
    public void onUserNotice(UserNoticeEvent event) {
        if (event.getMessage().length() > 0) {
            tray.displayMessage("DosBot - Twitch", String.format("%s\n%s", event.getSystemMsg(), event.getMessage().toString()), TrayIcon.MessageType.INFO);
        } else {
            if (event.getType().equals(UserNoticeEvent.MessageType.RAID)) {
                tray.displayMessage("DosBot - Twitch", String.format("%s", event.getSystemMsg()), TrayIcon.MessageType.WARNING);
            } else {
                tray.displayMessage("DosBot - Twitch", String.format("%s", event.getSystemMsg()), TrayIcon.MessageType.INFO);
            }
        }
    }
}
